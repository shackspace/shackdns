FROM mono:latest
# MANTAINER hasechris|gitlab.com/hasechris

RUN mkdir /shackDNS && mkdir /shackDNS/files
ENV CONFIG /shackDNS/files/config.cfg
ENV DNSDB /shackDNS/files/db.shack
ENV SHACKLESDB /shackDNS/files/shackles.db
ENV LEASESDB /shackDNS/files/leases.db
ENV BINDIP 0.0.0.0
COPY shackDNS.exe /shackDNS/shackDNS.exe
COPY Emitter.dll /shackDNS/Emitter.dll
COPY frontend/ /shackDNS/frontend
COPY mac-prefixes.tsv /shackDNS/mac-prefixes.tsv
COPY Newtonsoft.Json.dll /shackDNS/Newtonsoft.Json.dll
COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh
CMD ["/entrypoint.sh"]
